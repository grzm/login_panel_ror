### What is this repository for? ###
 
# Getting started

Would you like start it? Please, clone the repository, install gems and type:

	$ bundle install

Migrate the database:

	$ rails db:migrate

O course, run the test:

	$ rails test

If the test is correct:

	$ rails server

## Quick summary: 

	Login Panel ROR

Version: 

 	0.1
 
### How do I get set up? ###
 
# Summary of set up:

	Raspberry Pi 2 / Raspbian 9.1 Stretch

## Configuration:
 
 	Server: Puma 3.7

 	Gems:
 	Ruby 2.4.1
 	Rails 5.1.4
 	Sqlite3
 	...

### Who do I talk to? ###
 
Repo owner or admin

	grzegorz.michalski@invert8.pl

### How to use it? ###

Login at:
	
	User: Tester
	Pass: Tester
