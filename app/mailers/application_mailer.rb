class ApplicationMailer < ActionMailer::Base
  #default from: 'noreply@example.com'
  default from: 'noreply@invert8.com'
  layout 'mailer'
end
